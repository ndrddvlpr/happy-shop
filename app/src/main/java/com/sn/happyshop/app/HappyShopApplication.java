package com.sn.happyshop.app;

import android.app.Application;
import android.content.Context;
import android.support.multidex.MultiDex;

import com.facebook.drawee.backends.pipeline.Fresco;
import com.sn.happyshop.restapi.RestClient;

public class HappyShopApplication extends Application {

    private static RestClient restClient;
    static HappyShopApplication mInstance;

    @Override
    public void onCreate() {
        super.onCreate();
        Fresco.initialize(getApplicationContext());
        restClient = new RestClient();
        mInstance = this;
    }


    public static synchronized HappyShopApplication getInstance() {
        return mInstance;
    }


    @Override
    protected void attachBaseContext(Context base) {
        super.attachBaseContext(base);
        MultiDex.install(this);
    }

    public static RestClient getRestClient() {
        return restClient;
    }
}
