package com.sn.happyshop.screens;

import android.content.DialogInterface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;
import com.facebook.drawee.view.SimpleDraweeView;
import com.sn.R;
import com.sn.happyshop.custom.CustomTextView;
import com.sn.happyshop.objects.Product_Object;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;

/**
 * Created by parag.chauhan on 5/4/2016.
 */
public class CartScreen extends Base_Activity {

    @Bind(R.id.listView)
    SwipeMenuListView mListView;


    CartAdapter mCartAdapter;
    ArrayList<Product_Object> arrProduct;

    private Realm mReal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_cartscreen);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Cart");
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        arrProduct = new ArrayList<>();

        mCartAdapter = new CartAdapter(arrProduct);
        mListView.setAdapter(mCartAdapter);

        RealmConfiguration mRealConfiguration = new RealmConfiguration.Builder(this).build();
        mReal = Realm.getInstance(mRealConfiguration);

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {
                SwipeMenuItem deleteItem = new SwipeMenuItem(
                        getApplicationContext());
                deleteItem.setBackground(new ColorDrawable(getResources().getColor(R.color.headercolor)));
                deleteItem.setWidth(convertDpToPixel(90, getApplicationContext()));
                deleteItem.setIcon(R.mipmap.ic_delete);
                menu.addMenuItem(deleteItem);
            }
        };


        mListView.setMenuCreator(creator);
        mListView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        deleteAlert((Product_Object) mCartAdapter.getItem(position));
                        break;

                }
                return false;
            }
        });
        mListView.setOnSwipeListener(new SwipeMenuListView.OnSwipeListener() {

            @Override
            public void onSwipeStart(int position) {
                // swipe start
            }

            @Override
            public void onSwipeEnd(int position) {
                // swipe end
            }
        });
        mListView.setOnMenuStateChangeListener(new SwipeMenuListView.OnMenuStateChangeListener() {
            @Override
            public void onMenuOpen(int position) {
            }

            @Override
            public void onMenuClose(int position) {
            }
        });


        mListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Product_Object productObject = (Product_Object) mCartAdapter.getItem(i);
                Bundle bundle = new Bundle();
                bundle.putString("productName", productObject.getName());
                bundle.putInt("productId", productObject.getId());
                bundle.putString("category", productObject.getCategory());
                callActivity(ProductDetailsScreen.class, bundle);
            }
        });

        mListView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view,
                                           int position, long id) {
                Toast.makeText(getApplicationContext(), position + " long click", Toast.LENGTH_SHORT).show();
                return false;
            }
        });
        mListView.setSwipeDirection(SwipeMenuListView.DIRECTION_LEFT);
    }

    public void removeFromCart(Product_Object productObject) {
        RealmResults<Product_Object> results = mReal.where(Product_Object.class)
                .equalTo("id", productObject.getId())
                .findAll();
        if (results.size() > 0) {
            mReal.beginTransaction();
            results.get(0).deleteFromRealm();
            mReal.commitTransaction();
            fetchCartProducts();
        }
    }

    public void deleteAlert(final Product_Object productObject) {
        AlertDialog.Builder builder = new AlertDialog.Builder(CartScreen.this);

        builder.setTitle(getString(R.string.app_name));
        builder.setMessage("Are you sure to remove from cart?");

        builder.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                removeFromCart(productObject);
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alert = builder.create();
        alert.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchCartProducts();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void fetchCartProducts() {
        RealmResults<Product_Object> productList = mReal.where(Product_Object.class)
                .findAll();
        if (productList.size() == 0) {
            finish();
        }
        ArrayList<Product_Object> arrProductObjects = new ArrayList<>();
        arrProductObjects.addAll(productList);
        Collections.reverse(arrProductObjects);
        mCartAdapter.updateList(arrProductObjects);


    }

    public class CartAdapter extends BaseAdapter {
        ArrayList<Product_Object> mArrProduct;

        public CartAdapter(ArrayList<Product_Object> arrProduct) {
            mArrProduct = arrProduct;
        }

        //
        public void updateList(ArrayList<Product_Object> arrProduct) {
            mArrProduct = arrProduct;
            notifyDataSetChanged();
        }

        public List<Product_Object> getList() {
            return mArrProduct;
        }

        public int getCount() {
            return mArrProduct.size();
        }

        public Object getItem(int position) {
            return mArrProduct.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }


        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.screen_cart_list_item, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            final Product_Object productObject = mArrProduct.get(position);
            viewHolder.mTvProductName.setText(productObject.getName());
            viewHolder.mTvProductCategory.setText(productObject.getCategory());
            viewHolder.mTvProductDetails.setText(productObject.getDescription());
            viewHolder.mTvProductPrice.setText("$" + String.valueOf(productObject.getPrice()));
            viewHolder.mImgProduct.setImageURI(Uri.parse(productObject.getImg_url()));
            if (productObject.isUnder_sale()) {
                viewHolder.mTvProductOnSale.setVisibility(View.VISIBLE);
            }

            return convertView;
        }
    }

    public class ViewHolder {
        @Bind(R.id.tvProductName)
        CustomTextView mTvProductName;

        @Bind(R.id.tvProductCategory)
        CustomTextView mTvProductCategory;

        @Bind(R.id.tvProductPrice)
        CustomTextView mTvProductPrice;

        @Bind(R.id.tvProductDetails)
        CustomTextView mTvProductDetails;

        @Bind(R.id.imgProduct)
        SimpleDraweeView mImgProduct;

        @Bind(R.id.tvProductOnSale)
        CustomTextView mTvProductOnSale;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}

