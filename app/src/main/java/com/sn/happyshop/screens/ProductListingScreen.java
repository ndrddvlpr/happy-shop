package com.sn.happyshop.screens;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.GridView;

import com.facebook.drawee.view.SimpleDraweeView;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshGridView;
import com.sn.R;
import com.sn.happyshop.app.HappyShopApplication;
import com.sn.happyshop.custom.CustomTextView;
import com.sn.happyshop.objects.Product_Object;
import com.sn.happyshop.objects.Products_Object;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by parag.chauhan on 5/6/2016.
 */
public class ProductListingScreen extends Base_Activity {

    @Bind(R.id.gvProduct)
    PullToRefreshGridView pullToRefreshGridView;

    GridView mGridView;

    ProductAdapter mProductAdapter;
    ArrayList<Product_Object> arrProduct;

    String mSelectedCategory;
    int last_page = 1, current_page = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_productlisting);
        ButterKnife.bind(this);

        mSelectedCategory = getIntent().getStringExtra("category");

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(mSelectedCategory);
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        arrProduct = new ArrayList<>();

        mGridView = pullToRefreshGridView.getRefreshableView();
        mProductAdapter = new ProductAdapter(arrProduct);
        mGridView.setAdapter(mProductAdapter);
        pullToRefreshGridView.setMode(PullToRefreshBase.Mode.PULL_FROM_END);
        pullToRefreshGridView.setOnRefreshListener(new PullToRefreshBase.OnRefreshListener2<GridView>() {

            @Override
            public void onPullDownToRefresh(PullToRefreshBase<GridView> refreshView) {
            }

            @Override
            public void onPullUpToRefresh(PullToRefreshBase<GridView> refreshView) {
                if (last_page > current_page) {
                    current_page = current_page + 1;
                    fetchProductListing();
                }
                pullToRefreshGridView.onRefreshComplete();
            }

        });
        fetchProductListing();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void fetchProductListing() {
        if (isNetworkAvailable()) {
            showDialog();
            {
                Call<Products_Object> call = HappyShopApplication.getRestClient().getHappyShopAPIService().getProducts(mSelectedCategory, current_page);
                call.enqueue(new Callback<Products_Object>() {
                    @Override
                    public void onResponse(Call<Products_Object> call, Response<Products_Object> response) {
                        Products_Object response_object = response.body();
                        if (response_object.getArrProduct().size() > 0) {
                            last_page = current_page + 1;
                            arrProduct.addAll(response_object.getArrProduct());
                            mProductAdapter.updateList(arrProduct);
                        }
                        dismissDialog();
                    }

                    @Override
                    public void onFailure(Call<Products_Object> call, Throwable t) {
                        dismissDialog();
                        showToast(t.getMessage());
                    }
                });
            }
        }
    }

    public class ProductAdapter extends BaseAdapter {
        ArrayList<Product_Object> mArrProduct;

        public ProductAdapter(ArrayList<Product_Object> arrProduct) {
            mArrProduct = arrProduct;
        }

        //
        public void updateList(ArrayList<Product_Object> arrProduct) {
            mArrProduct = arrProduct;
            notifyDataSetChanged();
        }

        public ArrayList<Product_Object> getList() {
            return mArrProduct;
        }

        public int getCount() {
            return mArrProduct.size();
        }

        public Object getItem(int position) {
            return mArrProduct.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }


        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.screen_productlisting_list_item, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            final Product_Object productObject = mArrProduct.get(position);

            viewHolder.mImgProduct.setImageURI(Uri.parse(productObject.getImg_url()));
            viewHolder.mTvProductName.setText(productObject.getName());
            viewHolder.mTvProductPrice.setText("$" + String.valueOf(productObject.getPrice()));
            viewHolder.mTvProductOnSale.setVisibility(View.INVISIBLE);
            if (productObject.isUnder_sale()) {
                viewHolder.mTvProductOnSale.setVisibility(View.VISIBLE);
            }


            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("productName", productObject.getName());
                    bundle.putInt("productId", productObject.getId());
                    bundle.putString("category", mSelectedCategory);
                    callActivity(ProductDetailsScreen.class, bundle);
                }
            });
            return convertView;
        }

    }

    public class ViewHolder {

        @Bind(R.id.tvProductName)
        CustomTextView mTvProductName;

        @Bind(R.id.tvProductPrice)
        CustomTextView mTvProductPrice;

        @Bind(R.id.tvProductOnSale)
        CustomTextView mTvProductOnSale;

        //SimpleDraweeView mImgProduct ;
        @Bind(R.id.imgProduct)
        SimpleDraweeView mImgProduct;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
