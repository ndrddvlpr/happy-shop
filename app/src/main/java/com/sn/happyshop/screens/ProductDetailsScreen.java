package com.sn.happyshop.screens;

import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.MenuItem;
import android.view.View;

import com.facebook.drawee.view.SimpleDraweeView;
import com.sn.R;
import com.sn.happyshop.app.HappyShopApplication;
import com.sn.happyshop.custom.CustomButton;
import com.sn.happyshop.custom.CustomTextView;
import com.sn.happyshop.objects.CartResponse_Object;
import com.sn.happyshop.objects.ProductDetails_Object;
import com.sn.happyshop.objects.Product_Object;
import com.sn.happyshop.objects.Task;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/*
 * Created by parag.chauhan on 5/6/2016.
 */
public class ProductDetailsScreen extends Base_Activity {

    @Bind(R.id.tvProductName)
    CustomTextView mTvProductName;

    @Bind(R.id.tvProductPrice)
    CustomTextView mTvProductPrice;

    @Bind(R.id.tvProductDetails)
    CustomTextView mTvProductDetails;

    @Bind(R.id.btnAddtoCart)
    CustomButton mBtnAddtoCart;


    @Bind(R.id.imgProduct)
    SimpleDraweeView mImgProduct;

    @Bind(R.id.tvProductOnSale)
    CustomTextView mTvProductOnSale;

    int productId;

    private Realm mReal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_productdetails);

        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle(getIntent().getStringExtra("productName"));
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setHomeButtonEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);

        productId = getIntent().getIntExtra("productId", 0);

        RealmConfiguration mRealConfiguration = new RealmConfiguration.Builder(this).build();
        mReal = Realm.getInstance(mRealConfiguration);

        /*mBtnAddtoCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addToCartUsingAPI();
                   *//* if (isFromCart(productObject)) {
                        removeFromCart(productObject);
                        mBtnAddtoCart.setText(getString(R.string.addtocart));
                    } else {
                        addToCart(productObject);
                        mBtnAddtoCart.setText(getString(R.string.removetocart));
                    }*//*
            }
        });*/
        fetchProductDetails();
    }

    public void addToCart(Product_Object productObject) {
        mReal.beginTransaction();
        mReal.copyToRealmOrUpdate(productObject);
        mReal.commitTransaction();
    }


    public void removeFromCart(Product_Object productObject) {

        RealmResults<Product_Object> results = mReal.where(Product_Object.class)
                .equalTo("id", productObject.getId())
                .findAll();

        if (results.size() > 0) {
            mReal.beginTransaction();
            results.get(0).deleteFromRealm();
            mReal.commitTransaction();
            mBtnAddtoCart.setText(getString(R.string.addtocart));
        }

    }

    public boolean isFromCart(Product_Object productObject) {
        RealmResults<Product_Object> results = mReal.where(Product_Object.class)
                .equalTo("id", productObject.getId())
                .findAll();

        if (results.size() > 0) {
            return true;
        }
        return false;
    }

    public void addToCartUsingAPI(){
        if (isNetworkAvailable()) {
            showDialog();
            {//"line_items":[{"product_id":202,"quantity":3}]}'
                JSONObject jsonRequestObjcet = null;
                JSONArray jsonArray = null ;
                ArrayList<JSONObject> arrayList =null ;
                try{
                    jsonRequestObjcet = new JSONObject();
                    arrayList = new ArrayList<>();
                    jsonArray = new JSONArray();

                    JSONObject jsonObject = new JSONObject();
                    jsonObject.put("product_id",1);
                    jsonObject.put("quantity",1);
                    jsonArray.put(jsonObject);
                    arrayList.add(jsonObject);
                    jsonObject = new JSONObject();
                    jsonObject.put("product_id",2);
                    jsonObject.put("quantity",5);
                    jsonArray.put(jsonObject);
                    arrayList.add(jsonObject);
                   // jsonRequestObjcet.put("api_key",getResources().getString(R.string.apiKey));
                   // jsonRequestObjcet.put("line_items",jsonArray);



                }catch (Exception e){

                }

                if(jsonRequestObjcet == null){
                    return;
                }

                Task task = new Task(getResources().getString(R.string.apiKey),jsonArray.toString());

                Call<CartResponse_Object> call = HappyShopApplication.getRestClient().getHappyShopAPIService().addProductToCart(task);
                call.enqueue(new Callback<CartResponse_Object>() {
                    @Override
                    public void onResponse(Call<CartResponse_Object> call, Response<CartResponse_Object> response) {
                        CartResponse_Object response_object = response.body();
                        dismissDialog();
                        if (response_object != null) {
                         showToast(response_object.getArrCart().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<CartResponse_Object> call, Throwable t) {
                        dismissDialog();
                        showToast(t.getMessage());
                    }
                });
            }
        }
    }
    public void fetchProductDetails() {
        if (isNetworkAvailable()) {
            showDialog();
            {
                Call<ProductDetails_Object> call = HappyShopApplication.getRestClient().getHappyShopAPIService().getProductDetails(productId + ".json");
                call.enqueue(new Callback<ProductDetails_Object>() {
                    @Override
                    public void onResponse(Call<ProductDetails_Object> call, Response<ProductDetails_Object> response) {
                        ProductDetails_Object response_object = response.body();
                        dismissDialog();
                        if (response_object != null) {
                            fillValue(response_object.getProductDetails());
                        }
                    }

                    @Override
                    public void onFailure(Call<ProductDetails_Object> call, Throwable t) {
                        dismissDialog();
                        showToast(t.getMessage());
                    }
                });
            }
        }
    }

    public void fillValue(final Product_Object productObject) {

        if (productObject != null) {
            productObject.setCategory(getIntent().getStringExtra("category"));
            if (isFromCart(productObject)) {
                addToCart(productObject);
                mBtnAddtoCart.setText(getString(R.string.removetocart));
            } else {
                mBtnAddtoCart.setText(getString(R.string.addtocart));
            }
            mBtnAddtoCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    addToCartUsingAPI();
                   /* if (isFromCart(productObject)) {
                        removeFromCart(productObject);
                        mBtnAddtoCart.setText(getString(R.string.addtocart));
                    } else {
                        addToCart(productObject);
                        mBtnAddtoCart.setText(getString(R.string.removetocart));
                    }*/
                }
            });
            mTvProductName.setText(productObject.getName());
            mTvProductDetails.setText(productObject.getDescription());
            mTvProductPrice.setText("$" + String.valueOf(productObject.getPrice()));
            mImgProduct.setImageURI(Uri.parse(productObject.getImg_url()));
            if (productObject.isUnder_sale()) {
                mTvProductOnSale.setVisibility(View.VISIBLE);
            }
        }

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
