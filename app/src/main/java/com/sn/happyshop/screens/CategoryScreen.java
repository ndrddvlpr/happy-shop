package com.sn.happyshop.screens;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.ListView;

import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshListView;
import com.sn.R;
import com.sn.happyshop.app.HappyShopApplication;
import com.sn.happyshop.badgelibrary.ActionItemBadge;
import com.sn.happyshop.custom.CustomTextView;
import com.sn.happyshop.objects.Categories_Object;
import com.sn.happyshop.objects.Category_Object;
import com.sn.happyshop.objects.Product_Object;

import java.util.ArrayList;

import butterknife.Bind;
import butterknife.ButterKnife;
import io.realm.Realm;
import io.realm.RealmConfiguration;
import io.realm.RealmResults;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by parag.chauhan on 5/5/2016.
 */
public class CategoryScreen extends Base_Activity {

    @Bind(R.id.drawerLayout)
    DrawerLayout mDrawerLayout;

    @Bind(R.id.drawerList)
    ListView mDrawerList;

    private ActionBarDrawerToggle mDrawerToggle;


    String[] mDrawerMenuItem;

    @Bind(R.id.listView)
    PullToRefreshListView mListView;

    ListView mActualListView;
    CategoryAdapter mCategoryAdapter;
    ArrayList<Category_Object> arrCategory;

    private Realm mReal;

    RealmResults<Product_Object> cartProductList;

    int sizeOfCart = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_category);
        ButterKnife.bind(this);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Categories");
        actionBar.setDisplayShowTitleEnabled(true);
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setHomeButtonEnabled(true);

        mActualListView = mListView.getRefreshableView();
        mListView.setMode(PullToRefreshBase.Mode.DISABLED);
        arrCategory = new ArrayList<>();


        mCategoryAdapter = new CategoryAdapter(arrCategory);
        mActualListView.setAdapter(mCategoryAdapter);

        RealmConfiguration mRealConfiguration = new RealmConfiguration.Builder(this).build();
        mReal = Realm.getInstance(mRealConfiguration);

        mDrawerMenuItem = getResources().getStringArray(R.array.drawerMenuItem);

        mDrawerList.setAdapter(new ArrayAdapter<String>(this, R.layout.screen_category_drawer_list_item, mDrawerMenuItem));

        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        mDrawerToggle = new android.support.v7.app.ActionBarDrawerToggle(this, mDrawerLayout, R.string.drawer_open, R.string.drawer_close) {
            /* Called when drawer is closed */
            public void onDrawerClosed(View view) {
            }

            /* Called when a drawer is opened */
            public void onDrawerOpened(View drawerView) {
                //Put your code here
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        fetchCategory();
    }

    @Override
    protected void onResume() {
        super.onResume();
        fetchCartProducts();
    }

    public void fetchCartProducts() {
        cartProductList = mReal.where(Product_Object.class).findAll();
        sizeOfCart = cartProductList.size();
        invalidateOptionsMenu();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.cart_category, menu);
        menu.findItem(R.id.menu_cart).getActionView().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(sizeOfCart>0){
                    callActivity(CartScreen.class, null);
                }else{
                    showToast(getString(R.string.emptyalertmessage));
                }

            }
        });
        ActionItemBadge.update(menu.findItem(R.id.menu_cart),sizeOfCart);
        return true;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_cart:
                callActivity(CartScreen.class, null);
                return true;
            case android.R.id.home:
                if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
                    mDrawerLayout.closeDrawers();
                } else {
                    mDrawerLayout.openDrawer(GravityCompat.START);
                }
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public void fetchCategory() {
        if (isNetworkAvailable()) {
            showDialog();
            {
                Call<Categories_Object> call = HappyShopApplication.getRestClient().getHappyShopAPIService().getCategories();
                call.enqueue(new Callback<Categories_Object>() {
                    @Override
                    public void onResponse(Call<Categories_Object> call, Response<Categories_Object> response) {
                        Categories_Object response_object = response.body();
                        mCategoryAdapter.updateList(response_object.getArrCategory());
                        dismissDialog();
                    }

                    @Override
                    public void onFailure(Call<Categories_Object> call, Throwable t) {
                        dismissDialog();
                        showToast(t.getMessage());
                    }
                });
            }
        }
    }

    @Override
    public void onBackPressed() {
        if (mDrawerLayout.isDrawerOpen(GravityCompat.START)) {
            mDrawerLayout.closeDrawers();
            return;
        } else {
            finish();
        }
    }

    private class DrawerItemClickListener implements
            ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            mDrawerLayout.closeDrawers();
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    public class CategoryAdapter extends BaseAdapter {
        ArrayList<Category_Object> mArrCategory;

        public CategoryAdapter(ArrayList<Category_Object> arrCategory) {
            mArrCategory = arrCategory;
        }

        public void updateList(ArrayList<Category_Object> arrCategory) {
            mArrCategory = arrCategory;
            notifyDataSetChanged();
        }

        public ArrayList<Category_Object> getList() {
            return mArrCategory;
        }

        public int getCount() {
            return mArrCategory.size();
        }

        public Object getItem(int position) {
            return mArrCategory.get(position);
        }

        public long getItemId(int position) {
            return 0;
        }

        public View getView(int position, View convertView, ViewGroup parent) {

            final ViewHolder viewHolder;
            if (convertView == null) {
                convertView = getLayoutInflater().inflate(R.layout.screen_category_list_item, null);
                viewHolder = new ViewHolder(convertView);
                convertView.setTag(viewHolder);
            } else {
                viewHolder = (ViewHolder) convertView.getTag();
            }
            final Category_Object categoryObject = mArrCategory.get(position);
            viewHolder.mTvCatName.setText(categoryObject.getName() + " (" + String.valueOf(categoryObject.getProducts_count()) + ")");
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Bundle bundle = new Bundle();
                    bundle.putString("category", categoryObject.getName());
                    callActivity(ProductListingScreen.class, bundle);
                }
            });
            return convertView;
        }
    }

    public class ViewHolder {
        @Bind(R.id.tvCatName)
        CustomTextView mTvCatName;

        public ViewHolder(View view) {
            ButterKnife.bind(this, view);
        }
    }

}
