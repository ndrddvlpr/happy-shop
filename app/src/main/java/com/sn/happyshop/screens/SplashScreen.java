package com.sn.happyshop.screens;

import android.os.Bundle;
import android.os.Handler;

import com.sn.R;

/**
 * Created by parag.chauhan on 5/5/2016.
 */
public class SplashScreen extends Base_Activity {
    private Handler mHandler;
    private Runnable mRunnable;
    public int SPLASH_DURATION = 1500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.screen_splash);
        mHandler = new Handler();
        mRunnable = new Runnable() {
            @Override
            public void run() {
                callAndFinishActivity(CategoryScreen.class, null);
            }
        };
    }

    protected void onResume() {
        super.onResume();
        mHandler.postDelayed(mRunnable, SPLASH_DURATION);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mHandler.removeCallbacks(mRunnable);
    }
}
