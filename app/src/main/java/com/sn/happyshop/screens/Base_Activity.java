package com.sn.happyshop.screens;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ProgressBar;

import com.sn.R;
import com.sn.happyshop.custom.CustomTextView;


public class Base_Activity extends AppCompatActivity {
    static Activity mContext;
    static Dialog alertDialog;
    static View child;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mContext = this;
        child = getLayoutInflater().inflate(R.layout.custom_progress_bar, null);
        ProgressBar progressBar = (ProgressBar) child.findViewById(R.id.progressBar);
        progressBar.getIndeterminateDrawable().setColorFilter(
                getResources().getColor(R.color.whitecolor),
                android.graphics.PorterDuff.Mode.SRC_IN);
        CustomTextView tvMessage = (CustomTextView) child.findViewById(R.id.tvMessage);
        String msg = "";
        if (msg != null && msg.length() > 0) {
            tvMessage.setText(msg);
            tvMessage.setVisibility(View.VISIBLE);
        } else {
            tvMessage.setVisibility(View.GONE);
        }
        alertDialog = new Dialog(new ContextThemeWrapper(Base_Activity.this, R.style.AlertDialogCustom));
        alertDialog.setCancelable(false);
        alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        alertDialog.setContentView(child);
        alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS);
            getWindow().setStatusBarColor(getResources().getColor(R.color.headercolor));
        }
    }

    public static int dpToPx(Context context, int dp) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }




    public static void dismissDialog() {
        try {
            alertDialog.dismiss();
        } catch (Exception e) {
            System.out.print("Show dismiss=" + e);
        }
    }

    public static void showDialog() {

        try {
            alertDialog.show();
        } catch (Exception e) {
            child = mContext.getLayoutInflater().inflate(R.layout.custom_progress_bar, null);
            ProgressBar progressBar = (ProgressBar) child.findViewById(R.id.progressBar);
            progressBar.getIndeterminateDrawable().setColorFilter(
                    mContext.getResources().getColor(R.color.whitecolor),
                    android.graphics.PorterDuff.Mode.SRC_IN);
            CustomTextView tvMessage = (CustomTextView) child.findViewById(R.id.tvMessage);
            String msg = "";
            if (msg != null && msg.length() > 0) {
                tvMessage.setText(msg);
                tvMessage.setVisibility(View.VISIBLE);
            } else {
                tvMessage.setVisibility(View.GONE);
            }
            alertDialog = new Dialog(new ContextThemeWrapper(mContext, R.style.AlertDialogCustom));
            alertDialog.setCancelable(false);
            alertDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            alertDialog.setContentView(child);
            alertDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            alertDialog.show();
        }


    }

    @Override
    protected void onResume() {
        super.onResume();
        mContext = this ;
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    @Override
    protected void onStop() {
        super.onStop();
    }


    public static boolean isNetworkAvailable() {
        if (isNetworkAvailable(mContext)) {
            return true;
        } else {
            showToast(mContext.getString(R.string.networknotavailable));
            return false;
        }
    }

    public static void showToast(String msg) {
        // Toast.makeText(mContext,msg,Toast.LENGTH_SHORT).show();
        openMessageAlert(msg);
    }

    public static void showToastWithexit(String msg, Activity activity) {
        // Toast.makeText(mContext,msg,Toast.LENGTH_SHORT).show();
        openMessageAlertWithExit(msg, activity);
    }



    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivity = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);

        if (connectivity != null) {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();

            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    Log.i("Class", info[i].getState().toString());
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static int convertDpToPixel(float dp, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float px = dp * (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) px;
    }


    public static int convertPixelsToDp(float px, Context context) {
        Resources resources = context.getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        float dp = px / (metrics.densityDpi / DisplayMetrics.DENSITY_DEFAULT);
        return (int) dp;
    }

    public void openMessageToast(String msg) {
        openMessageAlert(msg);
    }

    public static void callActivity(Class mClass, Bundle mBundle) {
        Intent mIntent = new Intent(mContext, mClass);
        if (mBundle != null) {
            mIntent.putExtras(mBundle);
        }
        mContext.startActivity(mIntent);
    }

    public void callAndFinishActivity(Class mClass, Bundle mBundle) {
        Intent mIntent = new Intent(this, mClass);
        if (mBundle != null) {
            mIntent.putExtras(mBundle);
        }
        startActivity(mIntent);

        finishActivity();
    }

    public void finishActivity() {
        finish();
    }

    public static void openMessageAlertWithExit(String msg, final Activity activity) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(mContext.getString(R.string.app_name));
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                        activity.finish();
                    }
                })
               ;
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public static void openMessageAlert(String msg) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setTitle(mContext.getString(R.string.app_name));
        alertDialogBuilder
                .setMessage(msg)
                .setCancelable(false)
                .setPositiveButton(mContext.getString(R.string.ok), new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                })
              ;
        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }


}
