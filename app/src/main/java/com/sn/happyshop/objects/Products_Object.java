package com.sn.happyshop.objects;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

public class Products_Object {

    @SerializedName("products")
    public ArrayList<Product_Object> arrProduct;

    public ArrayList<Product_Object> getArrProduct() {
        return arrProduct;
    }

    public void setArrProduct(ArrayList<Product_Object> arrProduct) {
        this.arrProduct = arrProduct;
    }


}
