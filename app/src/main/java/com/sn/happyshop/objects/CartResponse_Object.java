package com.sn.happyshop.objects;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;

/**
 * Created by parag on 5/11/2016.
 */
public class CartResponse_Object {

    @SerializedName("cart_id")
    public int cartId;

    @SerializedName("line_items_count")
    public int cartItemsCount;

    @SerializedName("line_items")
    public ArrayList<Cart_Object> arrCart;

    public int getCartId() {
        return cartId;
    }

    public void setCartId(int cartId) {
        this.cartId = cartId;
    }

    public int getCartItemsCount() {
        return cartItemsCount;
    }

    public void setCartItemsCount(int cartItemsCount) {
        this.cartItemsCount = cartItemsCount;
    }

    public ArrayList<Cart_Object> getArrCart() {
        return arrCart;
    }

    public void setArrCart(ArrayList<Cart_Object> arrCart) {
        this.arrCart = arrCart;
    }
}
