package com.sn.happyshop.objects;


import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;


public class Product_Object extends RealmObject {

    public String img_url, name, description, category;

    @PrimaryKey
    public int id;


    public int price;
    public boolean under_sale;


    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImg_url() {
        return img_url;
    }

    public void setImg_url(String img_url) {
        this.img_url = img_url;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public boolean isUnder_sale() {
        return under_sale;
    }

    public void setUnder_sale(boolean under_sale) {
        this.under_sale = under_sale;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

}
