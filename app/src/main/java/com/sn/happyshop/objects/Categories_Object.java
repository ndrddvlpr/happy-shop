package com.sn.happyshop.objects;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;



public class Categories_Object {

    @SerializedName("categories")
    public ArrayList<Category_Object> arrCategory ;

    public ArrayList<Category_Object> getArrCategory() {
        return arrCategory;
    }

    public void setArrCategory(ArrayList<Category_Object> arrCategory) {
        this.arrCategory = arrCategory;
    }
}
