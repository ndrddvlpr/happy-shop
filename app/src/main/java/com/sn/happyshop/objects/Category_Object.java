package com.sn.happyshop.objects;





public class Category_Object {
    public String name ;
    public int products_count ;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getProducts_count() {
        return products_count;
    }

    public void setProducts_count(int products_count) {
        this.products_count = products_count;
    }
}
