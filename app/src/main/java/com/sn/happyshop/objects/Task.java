package com.sn.happyshop.objects;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by parag on 5/11/2016.
 */
//{"jsonObject"=>{"nameValuePairs"=>{"line_items"=>"[{\"product_id\":1,\"quantity\":1},{\"product_id\":2,\"quantity\":5}]", "api_key"=>"afa3f22f9d8a67e60b08aa95748df255"}}, "cart"=>{}}
public class Task {
    public String api_key;
    public String line_items;

    public Task(String apiKey ,String lineItems){
        this.api_key = apiKey;
        this.line_items = lineItems;

    }

}
