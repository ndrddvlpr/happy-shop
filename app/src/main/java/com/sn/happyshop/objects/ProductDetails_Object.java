package com.sn.happyshop.objects;

import com.google.gson.annotations.SerializedName;



public class ProductDetails_Object {
    @SerializedName("product")
    public Product_Object productDetails;

    public Product_Object getProductDetails() {
        return productDetails;
    }

    public void setProductDetails(Product_Object productDetails) {
        this.productDetails = productDetails;
    }

}
