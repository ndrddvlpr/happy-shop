package com.sn.happyshop.objects;

import com.google.gson.annotations.SerializedName;

/**
 * Created by parag on 5/11/2016.
 */
public class Cart_Object {
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getProductId() {
        return productId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public int id;
    @SerializedName("product_id")
    public int productId;
    public int quantity;
}
