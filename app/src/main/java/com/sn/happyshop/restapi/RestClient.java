package com.sn.happyshop.restapi;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RestClient {
    private HappyShopAPIServices happyShopAPIServices;
    public final static String BASE_URL = "http://sephora-mobile-takehome-apple.herokuapp.com/api/v1/";

    public static final String get_categories = BASE_URL + "categories.json";
    public static final String get_products = BASE_URL + "products.json";
    public static final String get_products_details = BASE_URL + "products/";
    public static final String add_product_tocart = BASE_URL + "carts.json";




    public RestClient() {

        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        // set your desired log level
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);
       //
        OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(httpClient.build())//For unable log
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        happyShopAPIServices = retrofit.create(HappyShopAPIServices.class);

    }

    public HappyShopAPIServices getHappyShopAPIService() {
        return happyShopAPIServices;
    }
}
