package com.sn.happyshop.restapi;


import com.sn.happyshop.objects.CartResponse_Object;
import com.sn.happyshop.objects.Categories_Object;
import com.sn.happyshop.objects.ProductDetails_Object;
import com.sn.happyshop.objects.Products_Object;
import com.sn.happyshop.objects.Task;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface HappyShopAPIServices {
    @GET(RestClient.get_categories)
    Call<Categories_Object> getCategories();

    @GET(RestClient.get_products)
    Call<Products_Object> getProducts(@Query("category") String categoryName, @Query("page") int page);

    @GET(RestClient.get_products_details + "{id}")
    Call<ProductDetails_Object> getProductDetails(@Path("id") String id);



    @POST(RestClient.add_product_tocart)
    Call<CartResponse_Object> addProductToCart(@Body() Task task);
    //Call<CartResponse_Object> addProductToCart(@Query("api_key")String apiKey,@Query("line_items") String jsonRequest);


}
